﻿using CalculadoraNumerosComplejos.Com.CalculadoraNumerosComplejos.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Registro de Cambios
/// 
/// Matrícula       Nombre
/// -----------------------------------------------------
/// 00105331        Miguel Angel Gil Rios
/// 60980 			Maricarmen Gonzalez Murillo
/// 60202			Thomas Alexander Epp Espinosa
/// 60314           Rodrigo Ramirez Cardenas
/// 60768			Pablo Cesar Roldan Perez
/// 59219	    	Amauri Javier Guzmán Hernández	
/// 59452			Andrea Hernández De Alba 
/// </summary>

namespace CalculadoraNumerosComplejos.Com.CalculadoraNumerosComplejos.Controller
{
    public class OperacionesNumeroComplejo
    {
        public NumeroComplejo Sumar(NumeroComplejo nc1, NumeroComplejo nc2)
        {
            NumeroComplejo nc3 = new NumeroComplejo();

            nc3.Real = nc1.Real + nc2.Real;
            nc3.Imaginario = nc1.Imaginario + nc2.Imaginario;
            return nc3;
        }

        public NumeroComplejo Restar(NumeroComplejo nc1, NumeroComplejo nc2)
        {
            NumeroComplejo nc3 = new NumeroComplejo();

            nc3.Real = nc1.Real - nc2.Real;
            nc3.Imaginario = nc1.Imaginario - nc2.Imaginario;
            return nc3;
        }

        public NumeroComplejo Multiplicar(NumeroComplejo nc1, NumeroComplejo nc2)
        {
            NumeroComplejo nc3 = new NumeroComplejo();

            nc3.Real = (nc1.Real * nc2.Real) - (nc1.Imaginario * nc2.Imaginario);
            nc3.Imaginario = (nc1.Real * nc2.Imaginario) + (nc1.Imaginario * nc2.Real);
            return nc3;
        }

        public NumeroComplejo Dividir(NumeroComplejo nc1, NumeroComplejo nc2)
        {
            double c2d2 = (nc2.Real * nc2.Real) + (nc2.Imaginario * nc2.Imaginario);
            NumeroComplejo nc3 = new NumeroComplejo();
            nc3.Real = c2d2 != 0 ? ((nc1.Real * nc2.Real) + (nc1.Imaginario * nc2.Imaginario)) / c2d2 : 0;
            nc3.Imaginario = c2d2 != 0 ? ((nc1.Imaginario * nc2.Real) - (nc1.Real * nc2.Imaginario)) / c2d2 : 0;
            return nc3;
        }
    }
}
