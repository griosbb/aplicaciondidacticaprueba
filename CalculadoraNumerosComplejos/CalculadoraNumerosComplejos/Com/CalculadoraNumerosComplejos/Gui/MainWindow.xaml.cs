﻿using CalculadoraNumerosComplejos.Com.CalculadoraNumerosComplejos.Controller;
using CalculadoraNumerosComplejos.Com.CalculadoraNumerosComplejos.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace CalculadoraNumerosComplejos
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        OperacionesNumeroComplejo onc;
        public MainWindow()
        {
            InitializeComponent();

            btnNCSumar.Click += BtnNCSumarAction;
            btnNCRestar.Click += BtnNCRestarAction;
            btnNCMultiplicar.Click += BtnNCMultiplicarAction;
            btnNCDividir.Click += BtnNCDividirAction;

            onc = new OperacionesNumeroComplejo();
        }

        private void BtnNCSumarAction(object sender, System.EventArgs e)
        {
            RealizarOperacionNumerosComplejos('+');
        }

        private void BtnNCRestarAction(object sender, System.EventArgs e)
        {
            RealizarOperacionNumerosComplejos('-');
        }

        private void BtnNCMultiplicarAction(object sender, System.EventArgs e)
        {
            RealizarOperacionNumerosComplejos('*');
        }

        private void BtnNCDividirAction(object sender, System.EventArgs e)
        {
            RealizarOperacionNumerosComplejos('/');
        }

        private void RealizarOperacionNumerosComplejos(char operacion)
        {
            NumeroComplejo nc1 = null;
            NumeroComplejo nc2 = null;
            NumeroComplejo nc3 = null;
            
            try
            {
                nc1 = LeerNumeroComplejo(txtNCReal1.Text, txtNCImg1.Text);
                nc2 = LeerNumeroComplejo(txtNCReal2.Text, txtNCImg2.Text);
                switch (operacion)
                {
                    case '+' :
                        //nc3 = onc.Sumar(nc1, nc2);
                        nc3 = nc1 + nc2;
                        break;
                    case '-':
                        nc3 = onc.Restar(nc1, nc2);
                        break;
                    case '*':
                        nc3 = onc.Multiplicar(nc1, nc2);
                        break;
                    case '/':
                        nc3 = onc.Dividir(nc1, nc2);
                        break;
                }
                if (nc3 != null)
                    txtNCResultado.Text = nc3.ToString();
                else
                    txtNCResultado.Text = "-";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private NumeroComplejo LeerNumeroComplejo(string strReal, string strImaginario)
        {
            return new NumeroComplejo(double.Parse(strReal), double.Parse(strImaginario));
        }
    }
}
