﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadoraNumerosComplejos.Com.CalculadoraNumerosComplejos.Model
{
    public class NumeroComplejo
    {
        private double real;
        private double imaginario;

        public NumeroComplejo() : this(0, 0)
        {

        }

        public NumeroComplejo(double real, double imaginario)
        {
            this.real = real;
            this.imaginario = imaginario;
        }

        public double Real
        {
            get { return real;  }
            set { real = value; }
        }

        public double Imaginario
        {
            get { return imaginario; }
            set { imaginario = value; }
        }

        
        public override String ToString()
        {
            return real + " + " + imaginario + "i";
        }

        public static NumeroComplejo operator + (NumeroComplejo nc1, NumeroComplejo nc2)
        {
            return new NumeroComplejo(nc1.Real + nc2.Real, nc1.Imaginario + nc2.Imaginario);
        }
    }
}
